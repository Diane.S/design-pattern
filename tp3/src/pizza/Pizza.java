package pizza;

import pizza.state.PatternState;
import pizza.state.StateDelivree;
import pizza.state.StateEnCuisson;
import pizza.state.StateEnFabrication;

public class Pizza  {

	private String name;
	private PatternState state;

	Pizza (String name){
		this.name = name;
		this.state = new StateEnFabrication(this);
	}

	public void changeState(PatternState state){
		this.state = state;
	}

	public PatternState getState() {
		return state;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void bake() throws Exception {
		if(state instanceof StateEnFabrication){
			changeState(new StateEnCuisson(this));
		}
		else {
			System.out.println("IMPOSSIBLE DE LA CUIRE CAR ELLE N'EST PAS EN COURS DE FABRICATION !!!\nElle est dans l'état :"+this.state.toString());
		}
	}

	public void deliver() throws Exception {
		if(state instanceof StateEnCuisson){
			changeState(new StateDelivree(this));
		}
		else {
			System.out.println("IMPOSSIBLE DE LA DELIVRER CAR ELLE N'ETAIT PAS EN CUISSON !!!\nElle est dans l'état :"+this.state.toString());
		}
	}
}

