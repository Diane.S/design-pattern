package pizza.state;

import pizza.Pizza;

public abstract class PatternState {
    Pizza m_pizza;

    PatternState(Pizza pizza){
        this.m_pizza = pizza;
    }

    public abstract String toString();
}
