package pizza.state;

import pizza.Pizza;

public class StateEnFabrication extends PatternState{

    public StateEnFabrication(Pizza pizza) {
        super(pizza);
    }

    @Override
    public String toString() {
        return "en cours de fabrication";
    }
}
