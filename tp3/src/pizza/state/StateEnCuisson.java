package pizza.state;

import pizza.Pizza;

public class StateEnCuisson extends PatternState{

    public StateEnCuisson(Pizza pizza) {
        super(pizza);
    }

    @Override
    public String toString() {
        return "en cuisson";
    }
}
