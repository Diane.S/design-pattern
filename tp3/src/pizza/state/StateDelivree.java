package pizza.state;

import pizza.Pizza;

public class StateDelivree extends PatternState{

    public StateDelivree(Pizza pizza) {
        super(pizza);
    }

    @Override
    public String toString() {
        return "délivrée";
    }
}
