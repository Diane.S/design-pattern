# TPs DesignPattern

Caroline PINTE ~ IN

Diane SEVIN ~ SI

## Rappels de l’énoncé 

L'objectif de ce TP est de comprendre et implémenter différents patrons de conception en Java.


## TP1 : Pattern Observer

On déclare l’interface `Observer` et on implémente la classe abstraite `Subject` tels que décrits dans l’énoncé.

La `MailBox` permet de notifier les changements, et le `CounterObserver` compte le nombre de messages reçus dans la `MailBox`. On les implémente également comme décrits dans l’énoncé. Enfin, on représente graphiquement la `MailBox` ainsi que les titres des mails reçus via la classe JLabel.


## TP2 : Etude du patron de conception Visiteur

Le programme étudié est le suivant :

	lire N
    tant_que N > 1 faire
        estImpaire := N % 2 
        si estImpaire alors 
            N := ( 3 * N ) + 1 
        sinon 
            N := N / 2 
        fin_si 
        imprimer N 
    fin_tant_que
    
Le diagramme de classes modélisant l'arbre syntaxique du pseudo-langage utilisé nous a été donné.

On effectue un dessin de l’arbre pour :

    Block(Assignment(Variable_Ref("x"),Integer_Value(10)),Print(Variable_Ref(" x")))

![TP2](./Images/TP2.png)

On nous transmet ensuite une partie de la grammaire abstraite.

**Définition des règles Bin_Expression, Integer_Value et Variable_Ref à partir du diagramme de classe :**

    Bin_Expression ::= (Integer_value | Variable_Ref) Operation (Bin_Expression | Integer_value | Variable_Ref 
    Integer_Value ::= Integer
    Variable_Ref ::= String

**Mise en oeuvre (Voir les fichiers fournis) :**

**1) Écrire le code Java d'un visiteur qui imprime chaque noeud de l'arbre à raison d'une ligne par Statement, sans indentation.**

Nous parcourons l’arbre réalisé par `SableCC3` jusqu’à arriver à un `Statement`, que nous pouvons alors imprimer sur la console. Pour l’instant, nous ne nous occupons pas de l’indexation, ce qui rend la réalisation plus simple. 

**2) Ajouter le comptage de la profondeur d'imbrication des blocs.**

Pour prendre en compte l’incrémentation, nous avons initialisé un compteur d’incrémentation à 0 au début. Avant de rentrer dans un `Statement`, nous incrémentons le compteur de 1, puis lorsque nous avons traité le `Statement` nous décrémentons le compteur.

**3) Mettre en oeuvre un interprète Evaluator de ce mini langage.**

Comme pour le printer que nous avons pu réaliser précédemment, nous allons parcourir l’arbre syntaxique du mini-langage. Puis, à chaque étape, nous allons modifier nos tableaux qui contiennent les valeurs pour qu’il puisse les interpréter. 
Nous avons alors réalisé 4 tableaux qui permettent d’associer des noms de variables à des valeurs mais ils permettent aussi la réalisation des opérations :

    List<String> nomVar = new ArrayList<String>() // permet de stocker en dur les noms de variables  
    List<Integer> value = new ArrayList<Integer>() // permet d’associer à l’indice du nom de variable du tableau précédent sa valeur en Integer.  
    List<Integer> variables = new ArrayList<Integer>() // permet d’avoir les variables qui nous intéresse lors de nos calculs.  
    List<String> op = new ArrayList<String>() // correspond aux opérateurs à traiter lors de l’opération.  

Vous trouverez ainsi le printer dans le fichier `Printer.java` et l’interpreteur dans le fichier `Interpreter.java`. Nous exécutons les deux dans notre `Main.java`.


## TP3 : Pattern Decorator et State

### Question 1 

Nous avons pu détecter plusieurs anomalies dans le diagramme fourni :
- Les deux tables Grande Pizza et Petite pizza ne sont pas nécessaires, il faudrait mieux ajouter un attribut taille à Pizza.  
- Pour ce qui est de Type de transport, il serait préférable de séparer la classe afin de pouvoir rajouter d’autres moyens de transport.  
- Nous avons remarqué la non présence de la gestion des réductions. (cf question 2)  
- Il faudrait envisager l’utilisation d’un Pattern State qui serait plus adapté pour les différents états de la commande. (cf question 3)  


### Question 2

Ce motif permet de grandement simplifier l’organisation d’un schéma complexe, le rend plus lisible et plus facile d’utilisation. En effet, un simple héritage nous demanderait de réimplémenter et modifier beaucoup de classes, alors que le système offert par ce motif demande uniquement l’ajout de nouvelles classes. Enfin, les nouvelles fonctionnalités seront très facilement ajoutables. Par exemple, nous pourrons ajouter `Happy Hours` à `Ristourne` et le comportement sera ainsi immédiatement présent.

![TP3_Q2](./Images/TP3_Q2.png)

### Question 3

Nous proposons ainsi la mise en oeuvre suivante :

![TP3_Q3](./Images/TP3_Q3.png)

Ce Pattern State permet ainsi de réaliser des actions en fonction de l’état de la commande. Vous retrouverez un début d’implémentation dans le src du tp3. 