package mailbox;
import java.util.ArrayList;
import java.util.List;

public class Subject {
    //attribut
    List<Observer> m_liste = new ArrayList<Observer>() ;

    //méthodes
    public void attach(Observer observer){
        m_liste.add(observer);
    }

    public void detach(Observer observer){
        m_liste.remove(observer);
    }

    public void notifyObservers(){
        for (int i = 0 ; i < m_liste.size() ; i++){
            m_liste.get(i).update(this);
        }
    }
}
