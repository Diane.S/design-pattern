package mailbox;
import javax.swing.JLabel;

public class MailObserver extends JLabel implements Observer{
    @Override
    public void update(Subject subject) {
        MailBox mailbox = (MailBox) subject;
        super.setText("MailObserver : "+mailbox.getLastMail().getBody()+"\n");
    }
}
