package mailbox;
import javax.swing.JLabel;

public class CounterObserver extends JLabel implements Observer {

    @Override
    public void update(Subject subject) {
        MailBox mailbox = (MailBox) subject;
        super.setText("CounterObserver : Le mail \""+ mailbox.getLastMail().getSubject() +"\" a été ajouté !!!\n");
    }
}
