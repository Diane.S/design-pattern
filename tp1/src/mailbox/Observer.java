package mailbox;

public interface Observer {
    void update(Subject subject);
}
