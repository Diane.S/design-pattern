package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import tp6.analysis.Analysis;
import tp6.node.*;


public class Interpreter extends AbstractInterpreter implements Analysis {
	List<String> nomVar = new ArrayList<String>();
	List<Integer> value = new ArrayList<Integer>();
	List<Integer> variables = new ArrayList<Integer>();
	List<String> op = new ArrayList<String>();

	@Override
	public void caseAAssignment(AAssignment node) {
		if (nomVar.indexOf(node.getVar().toString()) == -1){
			nomVar.add(node.getVar().toString());
			variables.clear();
			op.clear();
			node.getRhs().apply(this);
			if (op.isEmpty()){
				value.add(variables.get(0));
			}else {
				value.add(operator(variables.get(0),variables.get(2)));
				op.clear();
			}
		}
		else {
			variables.clear();
			node.getRhs().apply(this);
			int val = variables.get(0);
			variables.remove(0);
			variables.remove(0);
			while(op.size() > 0){
				val = operator(val,variables.get(0));
				op.remove(0);
				variables.remove(0);
				variables.remove(0);
			}
			value.set(nomVar.indexOf(node.getVar().toString()),val);
		}
	}

	@Override
	public void caseABinexpression(ABinexpression node) {
		node.getLExp().apply(this);
		variables.add(variables.get(0));
		node.getOperator().apply(this);
		node.getRExp().apply(this);
		variables.add(variables.get(0));
	}

	@Override
	public void caseABlock(ABlock node) {
		for (int i = 0 ; i < node.getStatement().size() ; i++){
			node.getStatement().get(i).apply(this);
		}
	}

	@Override
	public void caseAConditional(AConditional node) {
		variables.clear();
		node.getCond().apply(this);
		if (variables.get(0) != 0){
			node.getThenpart().apply(this);
		}
		else {
			node.getElsepart().apply(this);
		}
	}

	@Override
	public void caseAEqualOperatorlogique(AEqualOperatorlogique node) {

	}

	@Override
	public void caseAInfOperatorlogique(AInfOperatorlogique node) {
		op.add(node.getInf().toString());
	}

	@Override
	public void caseAIntegervalue(AIntegervalue node) {
		String s = node.getNumber().toString();
		variables.add(Integer.valueOf(s.substring(0,s.length()-1)));
	}

	@Override
	public void caseAMinusOperatorarith(AMinusOperatorarith node) {

	}

	@Override
	public void caseAMultiplierOperatorarith(AMultiplierOperatorarith node) {
		op.add(node.getMultiplier().toString());
	}

	@Override
	public void caseAPlusOperatorarith(APlusOperatorarith node) {
		op.add(node.getPlus().toString());
	}

	@Override
	public void caseAPrintS(APrintS node) {
		System.out.println(value.get(nomVar.indexOf(node.getValue().toString())));
	}

	@Override
	public void caseAReadS(AReadS node) {
		System.out.println(node.getVar() + "? "+ Main.entree);
		nomVar.add(node.getVar().toString());
		value.add(Integer.valueOf(Main.entree));
	}

	@Override
	public void caseASupOperatorlogique(ASupOperatorlogique node) {
		op.add(node.getSup().toString());
	}

	@Override
	public void caseAVariableref(AVariableref node) {
		int i = nomVar.indexOf(node.getIdentifier().toString());
		if (i == -1){
			variables.add(Integer.parseInt(node.getIdentifier().toString()));
		}
		else {
			variables.add(value.get(i));
		}
	}

	@Override
	public void caseAWhileS(AWhileS node) {
		variables.clear();
		node.getCond().apply(this);
		int a = variables.get(0);
		int b = variables.get(2);
		while (operator(a,b)==1){
			op.remove(0);
			node.getBody().apply(this);

			variables.clear();
			node.getCond().apply(this);
			a = variables.get(0);
		}
	}

	@Override
	public void caseADiviserOperatorarith(ADiviserOperatorarith node) {
		op.add(node.getDiv().toString());
	}

	@Override
	public void caseAModuloOperatorarith(AModuloOperatorarith node) {
		op.add(node.getModulo().toString());
	}

	public int operator(int a , int b){
		switch (op.get(0)) {
			case "> ":
				return BooleanToInt(a > b);
			case "< ":
				return BooleanToInt(a < b);
			case "% ":
				return a % b;
			case "/ ":
				return a / b;
			case "* ":
				return a*b;
			case "+ ":
				return a+b;
		}
		return 404;
	}

	public int BooleanToInt(Boolean bool){
		if (bool){
			return 1;
		}
		else {
			return 0;
		}
	}

	public void printVariables(){
		for (int i = 0 ; i < variables.size() ; i++){
			System.out.println("var "+i+" : "+ variables.get(i));
		}
	}

	public void printOp(){
		for (int i = 0 ; i < op.size() ; i++){
			System.out.println("op "+i+" : "+ op.get(i));
		}
	}

	public void printNomVar(){
		for (int i = 0 ; i < nomVar.size() ; i++){
			System.out.println("nomVar "+i+" : "+ nomVar.get(i));
		}
	}

	public void printValue(){
		for (int i = 0 ; i < value.size() ; i++){
			System.out.println("value "+i+" : "+ value.get(i));
		}
	}
}
